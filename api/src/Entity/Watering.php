<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\WateringRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WateringRepository::class)]
#[ApiResource]
class Watering
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $frequency = null;

    #[ORM\Column(length: 60)]
    private ?string $period = null;

    #[ORM\OneToMany(mappedBy: 'watering', targetEntity: Plant::class )]
    #[ORM\JoinColumn(nullable: true)]
    private Collection $plants;
    public function __construct()
    {
        $this->plants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFrequency(): ?int
    {
        return $this->frequency;
    }

    public function setFrequency(int $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }

    /**
     * @return Collection<int, Plant>
     */

    public function addPlant(Plant $plant): self
    {
        if (!$this->plants->contains($plant)) {
            $this->plants->add($plant);
            $plant->setWatering($this);
        }

        return $this;
    }

    public function removePlant(Plant $plant): self
    {
        if ($this->plants->removeElement($plant)) {
            // set the owning side to null (unless already changed)
            if ($plant->getWatering() === $this) {
                $plant->setWatering(null);
            }
        }

        return $this;
    }
}
