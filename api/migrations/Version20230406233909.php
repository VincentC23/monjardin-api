<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230406233909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plant ADD watering_id INT NOT NULL');
        $this->addSql('ALTER TABLE plant DROP watering');
        $this->addSql('ALTER TABLE plant ADD CONSTRAINT FK_AB030D72FA0020F0 FOREIGN KEY (watering_id) REFERENCES watering (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AB030D72FA0020F0 ON plant (watering_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE plant DROP CONSTRAINT FK_AB030D72FA0020F0');
        $this->addSql('DROP INDEX IDX_AB030D72FA0020F0');
        $this->addSql('ALTER TABLE plant ADD watering VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE plant DROP watering_id');
    }
}
