<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230324211528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE greeting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE types_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE plant_type (plant_id INT NOT NULL, type_id INT NOT NULL, PRIMARY KEY(plant_id, type_id))');
        $this->addSql('CREATE INDEX IDX_EF36973B1D935652 ON plant_type (plant_id)');
        $this->addSql('CREATE INDEX IDX_EF36973BC54C8C93 ON plant_type (type_id)');
        $this->addSql('CREATE TABLE type (id INT NOT NULL, name VARCHAR(125) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE plant_type ADD CONSTRAINT FK_EF36973B1D935652 FOREIGN KEY (plant_id) REFERENCES plant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE plant_type ADD CONSTRAINT FK_EF36973BC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE greeting');
        $this->addSql('DROP TABLE types');
        $this->addSql('ALTER TABLE plant DROP types');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE type_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE greeting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE types_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE greeting (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE types (id INT NOT NULL, name VARCHAR(120) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE plant_type DROP CONSTRAINT FK_EF36973B1D935652');
        $this->addSql('ALTER TABLE plant_type DROP CONSTRAINT FK_EF36973BC54C8C93');
        $this->addSql('DROP TABLE plant_type');
        $this->addSql('DROP TABLE type');
        $this->addSql('ALTER TABLE plant ADD types TEXT NOT NULL');
        $this->addSql('COMMENT ON COLUMN plant.types IS \'(DC2Type:array)\'');
    }
}
